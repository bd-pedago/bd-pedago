Documentation des serveurs pédagogiques de bases de données `bd-pedago`
=========================================================

<!-- markdownlint-disable MD004-->
- [Documentation des serveurs pédagogiques de bases de données `bd-pedago`](#documentation-des-serveurs-pédagogiques-de-bases-de-données-bd-pedago)
  - [Documentation du serveur PostgreSQL](#documentation-du-serveur-postgresql)
    - [Connection au serveur](#connection-au-serveur)
    - [Les interfaces clients PostgreSQL](#les-interfaces-clients-postgresql)
      - [`psql`](#psql)
      - [DBeaver](#dbeaver)
      - [CloudBeaver](#cloudbeaver)
    - [Gestion des schémas en PostgreSQL](#gestion-des-schémas-en-postgresql)
      - [Le `search_path`](#le-search_path)
      - [Le schéma public](#le-schéma-public)
      - [Modifier le  `search_path`](#modifier-le-search_path)
    - [Accéder à d'autres bases de données depuis la sienne](#accéder-à-dautres-bases-de-données-depuis-la-sienne)
  - [Documentation du serveur MongoDB](#documentation-du-serveur-mongodb)
    - [Connection au serveur](#connection-au-serveur-1)
      - [Avec le tunnel SSH](#avec-le-tunnel-ssh)
      - [Connection directe depuis Eduroam ou le VPN](#connection-directe-depuis-eduroam-ou-le-vpn)
    - [Installation locale](#installation-locale)
  - [Annexes](#annexes)
    - [PostgreSQL : exemple complet avec `search_path`](#postgresql--exemple-complet-avec-search_path)
    - [PostgreSQL : exemple complet FDW](#postgresql--exemple-complet-fdw)
    - [PostgreSQL : configuration du tunnelling ssh (obsolete depuis le routage public internet)](#postgresql--configuration-du-tunnelling-ssh-obsolete-depuis-le-routage-public-internet)
      - [Configuration de la redirection en ligne de commande sous Linux](#configuration-de-la-redirection-en-ligne-de-commande-sous-linux)
      - [Configuration de la redirection via putty sous windows](#configuration-de-la-redirection-via-putty-sous-windows)
      - [Configuration de la redirection directement dans DBeaver](#configuration-de-la-redirection-directement-dans-dbeaver)
<!-- markdownlint-enable MD004-->

Le point d'entrée aux serveurs est `bd-pedago.univ-lyon1.fr`.
La machine est **publiquement routée** sur internet et EDUROAM mais **seul** le port PostgreSQL/5432 est visible. **Attention**  le service n'est **pas** accessible depuis les réseaux wifi UCBL-Portail et eduscol

Si un service n'est accessible **que** depuis le campus (autres ports) :

- utiliser le réseau EDUROAM en wifi cf. <https://etu.univ-lyon1.fr/guide-wifi-eduroam-694268.kjsp> ou <https://cat.eduroam.org/>
- utiliser un tunnel SSH ou le VPN depuis l'exterieur, cf. <https://documentation.univ-lyon1.fr/>

Documentation du serveur PostgreSQL
-----------------------------------

**Documentation de référence : <https://www.postgresql.org/docs/current/index.html>**

### Connection au serveur

Pour se connecter `bd-pedago.univ-lyon1.fr`, avec `Login` votre identifiant Lyon 1 de la forme `pAB12345` (avec `AB` votre année de première inscription) et le mot de passe `MotDePasse` à 12 caractères donné dans <https://tomuss.univ-lyon1.fr/> dans une colonne `Password_PostgreSQL`.

### Les interfaces clients PostgreSQL

Nous recommandons d'utiliser la ligne de commande interactive `psql`, le client local DBeaver (à installer sur votre poste, déjà présent sous Linux sur les postes de l'université) ou le serveur CloudBeaver (interface web, pas d'installation).

#### `psql`

Utiliser l'une des deux méthodes suivantes pour vous connecter :

```bash
# en tapant le mot de passe à l'inve intéractive
psql -h bd-pedago.univ-lyon1.fr -U Login -d BaseDeDonnees

# en mettant le mot de passe dans une variable d'environnement
PGPASSWORD=MotDePasse psql -h bd-pedago.univ-lyon1.fr -U Login -d BaseDeDonnees
```

Si vous ne précisez pas `-d BaseDeDonnees`, vous essaierez de vous connecter par défaut dans la base qui a le même nom que le login (ici `Login`).

Pour _éviter de saisir à chaque fois votre mot de passe_ : créer un script `.sh` avec la commande précédente ou utiliser le fichier `.pgpass` qui permet de saisir vos information de connection, par exemple (voir <https://www.postgresql.org/docs/current/libpq-pgpass.html>)

```pgpass
bd-pedago.univ-lyon1.fr:5432:pgrthion:pgrthion:MotDePasse
bd-pedago.univ-lyon1.fr:5432:pedago:pgrthion:MotDePasse
```

Configurer **en particulier** la pagination et l'éditeur utilisé accessible via `\e` par `psql` :

- soit avec `select-editor` sous Ubuntu
- soit via la variable d'environnement `PSQL_EDITOR`

Un exemple de  configuration du fichier `$HOME/.psqlrc` est [accessible dans ce dépôt](./.psqlrc). Il est **très recommandé** de l'utiliser. Pour aller plus loin, vous pouvez [voir la configuration détaillée et expliquée de DImitri Fontaine](https://tapoueh.org/blog/2017/12/setting-up-psql-the-postgresql-cli/), un contributeur de PostgreSQL.
Voir [la doc officielle](https://www.postgresql.org/docs/current/app-psql.html).

#### DBeaver

DBeaver Community Edition <https://dbeaver.io/> est installé sur les postes Linux de l'établissement. La connexion se configure avec les mêmes informations, voir l'exemple ci-dessous avec le compte `rthion`. **Pensez bien à configurer le nom de la base de donnéess**, qui est le même que votre login.

![Configuration DBeaver](./img/DBeaver_connection_base.png)

#### CloudBeaver

Un équivalent allégé de DBeaver, nommé CloudBeaver, est installé en mode serveur. Il est accessible à l'adresse <https://cloudbeaver.univ-lyon1.fr/>. Une documentation pour configurer votre connection est accessible <https://documentation.univ-lyon1.fr/sgbd/cloudbeaver>.

### Gestion des schémas en PostgreSQL

**Attention** dans PostgreSQL, l'organisation est **Base de données > Schémas > Tables**.
Il est recommandé de créer un nouveau schéma pour chaque application, c'est-à-dire en pratique pour chaque nouveau sujet de TP/Projet.

Un exemple complet d'utilisation du search_path est donné [dans l'annexe](#Exemple-complet-avec-search_path)

#### Le `search_path`

Postgres va chercher les noms dans les schémas avec un mecanismes `search_path` similaire à celui des systèmes de gestion de fichiers.
Voir <https://www.postgresql.org/docs/current/ddl-schemas.html#DDL-SCHEMAS-PATH>

```sql
show search_path;

--      search_path
-- ------------------------
-- "$user", public
```

Par défaut, postgres va donc chercher une table d'abord dans le schéma éponyme de l'utilisateur `$user` puis dans le schéma `public`.

#### Le schéma public

Par défaut, si vous créez une table sans préciser de schéma, postgres va essayer de créer dans la table dans l'ordre du `search_path`.
Le schéma `public` étant toujours créé, si vous n'avez pas crée de schéma éponyme à votre nom d'utilisateur, c'est dans `public` que la table sera créee.

**Tous les utilisateurs ont les droits de création par défaut dans tous les schémas publics de tous les utilisateurs**. Ce comportement sera modifié dans le futur sur l'instance.

#### Modifier le  `search_path`

Vous pouvez modifier le  `search_path` avec la commande, par exemple pour y ajouter un schéma nouvellement crée. Cette modification sera valable **pour la session en cours**

```sql
SET search_path TO myschema,public;
```

Si vous voulez altérer  `search_path` pour **toutes les futures sessions d'une base ou d'un utilisateur** utilisez une des commandes suivantes (voir <https://stackoverflow.com/questions/2875610/permanently-set-postgresql-schema-path> ).

```sql
-- /!\ privilégiez cette solution /!\
-- au niveau utilisateur, pour toutes les sessions de cet utilisateur
-- quelle que soit la base
ALTER ROLE <role_name> SET search_path TO schema1,schema2;

-- au niveau base de données
-- tous les utilsateurs qui s'y connectent auront ce search_path
ALTER DATABASE <database> SET search_path TO schema1,schema2;
```

### Accéder à d'autres bases de données depuis la sienne

Tous les utilisateurs ont droit **d'usage** dans tous les schémas de la base nommée `pedago` via le rôle `pedago-reader`. Pour lire, il suffit donc de se connecter à cette base :

- en psql depuis une autre base, la commande est  `\c pedago`
- à la connection directement `psql -h bd-pedago.univ-lyon1.fr -U LoginPostgres -d pedago`

Pour croiser des données entre plusieurs bases de données, il faut utiliser l'extension *Foreign Data Wrapper* (FDW) qui permet d'accéder à des bases distances (potentiellement, sur un autre serveur).
L'extension a été activée dans chaque base utilisateur. Pour accéder à des tables se fait en trois étapes :

 1. créer le serveur
 2. créer la correspondance utilisateur
 3. importer les tables

 Voir le script en annexe

Documentation

- <https://www.postgresql.org/docs/current/postgres-fdw.html>
- <https://www.postgresql.org/docs/current/sql-createserver.html>
- <https://www.postgresql.org/docs/current/sql-createusermapping.html>
- <https://www.postgresql.org/docs/current/sql-importforeignschema.html>
- <https://www.postgresql.org/docs/current/sql-createforeigntable.html>

Documentation du serveur MongoDB
--------------------------------

Les bases de données accessibles sur le serveur sont les suivantes :

- `grades` : une base de notes académiques
- `zips` : les données du recensement des États-Unis par codes postaux
- `restaurants` : des restaurants à Manhattan
- `neighborhoods` : les quartiers de Manhattan et leur définition géométrique

### Connection au serveur

**Attention, le port 27017 de MongoDB n'est pas ouvert sur internet**.
Il faut donc **obligatoirement** utiliser Eduroam, le VPN, ou un tunnel SSH.

#### Avec le tunnel SSH

La commande pour créer le tunnel est la suivante

```bash
# avec loginUCBL votre identifiant p1234567

# -f met ssh en tâche de fond
# -N n'exécute pas de commande une fois connecté
# -L bind_address:port:host:hostport redirige ici bd-pedago.univ-lyon1.fr:27017 sur localhost:27017
ssh -f -N -L localhost:27017:bd-pedago.univ-lyon1.fr:27017 loginUCBL@linuxetu.univ-lyon1.fr
```

Vous pouvez maintenant vous connecter sur la base  `base` avec l'utilisateur `login` et son mot de passe `password` définis dans la base `auth-db` (la base d'authentification n'est pas nécessairement celle ou on se connecte), la commande est la suivante :

```bash
# notez le localhost à cause du tunnel SSH
mongo -u "login" -p "password"  --authenticationDatabase "auth-db" "mongodb://localhost:27017/base"
```

Les informations `base`, `login`, `password` et `auth-db` sont données en TP.

#### Connection directe depuis Eduroam ou le VPN

Si vous n'avez pas besoin de SSH, la commande est la suivante

```bash
mongo -u "login" -p "password"  --authenticationDatabase "auth-db" "mongodb://bd-pedago.univ-lyon1.fr:27017/base"
```

### Installation locale

Si vous souhaitez reproduire l'environnement fourni, vous pouvez suivre les guides d'installation officiels <https://docs.mongodb.com/manual/installation/>  et installer MongoDB sur votre propre machine.
Il n'y a quasiement aucun `tuning` ou spécificité sur l'instance que nous exploitons, vous pourrez donc facilement reproduire les TPs sur votre propre serveur.
Vous aurez simplement besoin des jeux de données, voici comment les télécharger :

```bash
wget https://raw.githubusercontent.com/ozlerhakan/mongodb-json-files/master/datasets/grades.json
wget http://media.mongodb.org/zips.json
wget https://raw.githubusercontent.com/mongodb/docs-assets/geospatial/restaurants.json
wget https://raw.githubusercontent.com/mongodb/docs-assets/geospatial/neighborhoods.json
```

Ensuite, il suffit de les importer dans votre serveur, ici dans une base `mif04` sans authentification (ce qui est le cas d'une installation par défaut : pas de mot de passe mais accès local uniquement) :

```bash
mongoimport --host localhost --db mif04 --collection grades --drop --file grades.json
mongoimport --host localhost --db mif04 --collection zips --drop --file zips.json
mongoimport --host localhost --db mif04 --collection restaurants --drop --file restaurants.json
mongoimport --host localhost --db mif04 --collection neighborhoods --drop --file neighborhoods.json
```

Annexes
-------

### PostgreSQL : exemple complet avec `search_path`

On se connecte ici avec `psql -U pgrthion -h bd-pedago.univ-lyon1.fr` :

```sql
show search_path ;
--   search_path
-- -----------------
-- "$user", public
-- (1 row)

\dn
--   List of schemas
--   Name  |  Owner
-- --------+----------
--  public | postgres

\dt
-- Did not find any relations.

CREATE TABLE test(id int);
-- CREATE TABLE
\dt
--         List of relations
--  Schema | Name | Type  |  Owner
-- --------+------+-------+----------
--  public | test | table | pgrthion
-- (1 row)

-- création dans public, car pas de schéma pgrthion de crée

CREATE SCHEMA pgrthion;
-- CREATE SCHEMA

\dt
--         List of relations
--  Schema | Name | Type  |  Owner
-- --------+------+-------+----------
--  public | test | table | pgrthion
-- (1 row)

CREATE TABLE test2(id int);
-- CREATE TABLE

\dt
--           List of relations
--   Schema  | Name  | Type  |  Owner
-- ----------+-------+-------+----------
--  pgrthion | test2 | table | pgrthion
--  public   | test  | table | pgrthion
-- (2 rows)

CREATE SCHEMA test;
-- CREATE SCHEMA

\dn
--    List of schemas
--    Name   |  Owner
-- ----------+----------
--  pgrthion | pgrthion
--  public   | postgres
--  test     | pgrthion
-- (3 rows)

-- /!\ attention test est crée mais n'est pas dans le path !

CREATE TABLE test.test3(id int);
-- CREATE TABLE

\dt
--           List of relations
--   Schema  | Name  | Type  |  Owner
-- ----------+-------+-------+----------
--  pgrthion | test2 | table | pgrthion
--  public   | test  | table | pgrthion
-- (2 rows)

-- on ne voit pas test3 !

ALTER ROLE pgrthion SET search_path TO "$user",test;
-- ALTER ROLE

\dt
--           List of relations
--   Schema  | Name  | Type  |  Owner
-- ----------+-------+-------+----------
--  pgrthion | test2 | table | pgrthion
--  public   | test  | table | pgrthion
-- (2 rows)

-- /!\ SERA VISIBLE à la prochaine connexion
\q
```

On se connecte à nouveau avec `psql -U pgrthion -h bd-pedago.univ-lyon1.fr`

```sql
\dt
--           List of relations
--   Schema  | Name  | Type  |  Owner
-- ----------+-------+-------+----------
--  pgrthion | test2 | table | pgrthion
--  test     | test3 | table | pgrthion
-- (2 rows)

-- /!\ on voit la table test3, mais plus test qui est dans public car public n'est plus dans le path
```

### PostgreSQL : exemple complet FDW

Le script ci-dessous vous donne toutes les commandes pour importer des tables d'une autre base de données

```sql
-- on crée le serveur "distant", ici vers la base 'pedago'
-- on lève ici les droits d'écritures (que les utilisateurs n'ont pas de toute façon)
CREATE SERVER IF NOT EXISTS ro_local_bd_pedago
  FOREIGN DATA WRAPPER postgres_fdw
  OPTIONS (host 'bd-pedago.univ-lyon1.fr', port '5432', dbname 'pedago',  updatable 'false');

-- pour annuler
-- DROP SERVER IF EXISTS ro_local_bd_pedago;

--  on dit que l'utilisateur local LoginPostgres accède au distant via le même compte LoginPostgres : il faut préciser le mot de passe
CREATE USER MAPPING IF NOT EXISTS FOR LoginPostgres SERVER ro_local_bd_pedago OPTIONS (user 'LoginPostgres', password 'MotDePasse');

-- pour annuler
-- DROP USER MAPPING IF EXISTS FOR LoginPostgres SERVER ro_local_bd_pedago;

-- enfin pour ajouter, ici DEUX TABLES DU SCHEMA mif04_xml
IMPORT FOREIGN SCHEMA mif04_xml
  LIMIT TO (arbres_foret_2011, documentation_2011)
  FROM SERVER ro_local_bd_pedago INTO public;

-- on teste
SELECT COUNT(*)
FROM arbres_foret_2011;

-- PAS LA PEINE DE RECRER ro_local_bd_pedago et son mapping par la suite si on veut ajouter d'autres tables
```

Pour lister les tables importées:

```sql
\det
```

### PostgreSQL : configuration du tunnelling ssh (obsolete depuis le routage public internet)

#### Configuration de la redirection en ligne de commande sous Linux

Pour créer un tunnel qui redirige (option `-L`) le port 5432 de `bd-pedago.univ-lyon1.fr` sur le port 5432 de votre `localhost` en tâche de fond (option `-f`) et qui n'exécute aucune commmande (option `-N`), la commande est la suivante :

```bash
ssh -f -N -L localhost:5432:bd-pedago.univ-lyon1.fr:5432 LoginUCBL@linuxetu.univ-lyon1.fr
# le prompt va vous demander votre pass UCBL, pas votre pass PotsgreSQL
```

Ensuite, vous pouvez vous connecter au port local avec la commande suivante et saisir votre mot de passe PostgreSQL

```bash
psql -p 5432 -U LoginPostgres -h localhost
```

**Si vous avez déjà PostgreSQL d'installé en local, alors changez de port local**.

#### Configuration de la redirection via putty sous windows

En utilisant <https://wwww.putty.org>, il faut configurer la redirection dans les options SSH/Tunnels, voir les captures ci-dessous. Pensez à sauvegarder votre configuration.

![Configuration tunnel Putty 1/2](./img/Putty_host.png)

![Configuration tunnel Putty 2/2](./img/Putty_tunnel.png)

#### Configuration de la redirection directement dans DBeaver

Le client recommandé <https://dbeaver.io/> permet de configurer directement un tunnel SSH sans passer par Putty, dans l'onglet SSH lors de la création de la connection.

![Configuration tunnel DBeaver 1/2](./img/DBeaver_connection.png)

![Configuration tunnel DBeaver 2/2](./img/DBeaver_tunnel.png)
